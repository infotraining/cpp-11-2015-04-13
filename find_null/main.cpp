#include <iostream>
#include <vector>
#include <iterator>
#include <memory>
#include <cassert>
#include <iterator>

using namespace std;

template <typename Container>
auto find_null(Container& container) -> decltype(begin(container))
{
    auto it = begin(container);
    for(; it != end(container); ++it)
    {
        if (*it == nullptr)
            return it;
    }

    return it;
}

template<class C>
constexpr auto cbegin( const C& c ) -> decltype(std::begin(c))
{
    return std::begin(c);
}

int main()
{
    // find_null: Given a container of pointers, return an
    // iterator to the first null pointer (or the end
    // iterator if none is found)

    const vector<int*> vec_ptr = { new int{10}, nullptr, new int{20} };

    auto where_null1 = find_null(vec_ptr);

    assert(distance(vec_ptr.begin(), where_null1) == 1);

    int* tab[] = { vec_ptr[0], vec_ptr[0], vec_ptr[2], nullptr };

    auto where_null2 = find_null(tab);

    assert(distance(begin(tab), where_null2) == 3);

    vector<shared_ptr<int>> vec_sptr = { make_shared<int>(10), nullptr, make_shared<int>(20) };

    auto where_null3 = find_null(vec_sptr);

    assert(distance<decltype(where_null3)>(vec_sptr.begin(), where_null3) == 1);
}
