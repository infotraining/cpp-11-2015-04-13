#include <iostream>
#include <type_traits>
#include <typeinfo>
#include <set>
#include <array>

using namespace std;

enum Coffee : uint8_t { espresso = 1, latte, american = 55 };

enum class DayOfWeek : unsigned char { Mon, Tue, Wed, Thd, Fri, Sat, Sun };

template <typename T>
using DictDesc = set<T, greater<T>>;


template <typename T>
using SizedArray = array<T, 255>;

int main()
{
    cout << typeid(underlying_type_t<Coffee>).name() << endl;

    DayOfWeek dw = DayOfWeek::Tue;

    dw = static_cast<DayOfWeek>(1);

    if (dw == DayOfWeek::Tue)
        cout << "dw is Tue";

    int value = static_cast<int>(DayOfWeek::Sat);

    cout << " value = " << value << endl;

    cout << "\n";

    DictDesc<string> dict1 = { "one", "two", "three", "four" };

    for(const auto& key : dict1)
        cout << key << endl;

    SizedArray<int> arr = { 1, 2, 3, 4, 5, 6 };
}
