#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<string> vec = { "1", "2", "3", "4" };

    cout << "vec: ";
    for(const auto& item : vec)
        cout << item << " ";
    cout << endl;

    for(auto it = vec.begin(); it != vec.end(); ++it)
    {
        auto item = *it;
        cout << item << " ";
    }
    cout << endl;

    int tab[] = {1, 2, 3, 4, 5};

    cout << "tab: ";
    for(const auto& item : tab)
        cout << item << " ";
    cout << endl;


    for(auto it = begin(tab); it != end(tab); ++it)
    {
        auto item = *it;
        cout << item << " ";
    }
    cout << endl;

    vector<bool> bits = { 1, 0, 0, 0 };

    for(auto&& bit : bits)
        bit.flip();

    cout << "bits: ";
    for(auto&& bit : bits)
        cout << bit << " ";
    cout << endl;

    for(const auto& item : { "one", "two", "three" })
    {
        cout << item << endl;
    }
}
