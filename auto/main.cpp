#include <iostream>
#include <set>
#include <string>
#include <vector>

using namespace std;

// 1st case
//template <typename T>
//void deduce_type(T& t)
//{

//}

// 2nd case
template <typename T>
void deduce_type(T&& t)
{

}

// 3rd case
//template <typename T>
//void deduce_type(T t)
//{

//}

struct Expl
{
    Expl() {};

    explicit Expl(const Expl& e)
    {}
};

int main()
{
    deduce_type(std::initializer_list<int>{1, 2, 3});  // special case

    const vector<string> dict = { "one", "two", "three", "four" };


    for(auto it = dict.begin(); it != dict.end(); ++it)
        cout << *it << endl;

    Expl e1;

    auto e2(e1);
}
