#include <iostream>
#include <cstddef>

using namespace std;

void foo(long n)
{
    cout << "foo(long)" << endl;
}

void foo(long* ptr)
{
    cout << "foo(ptr)" << endl;
}

void foo(nullptr_t)
{
    cout << "foo(nullptr)" << endl;
}

int main()
{
    long x = 10;
    long* ptr1 {};

    if (ptr1 == nullptr)
        cout << "ptr1 is nullptr" << endl;

    foo(x);
    foo(ptr1);

    foo(nullptr);
}
