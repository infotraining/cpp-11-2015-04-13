#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <functional>
#include <set>
#include <memory>

using namespace std;

class Lambda_31234124
{
    const int threshold;

public:
    Lambda_31234124(int threshold) : threshold(threshold)
    {}

    bool operator()(int x)
    {
        return x > threshold;
    }
};

class Aggregator
{
    int sum_{};
public:
    Aggregator() { cout << "Aggregator()" << endl; }
    ~Aggregator() { cout << "~Aggregator(sum: " << sum_ << ")" << endl; }

    void cumulate(int arg)
    {
        cout << "Aggregator::cumulate(" << arg << ")" << endl;
        sum_ += arg;
    }

    int sum() const
    {
        return sum_;
    }
};

function<int(int)> function_functory(int x)
{
    return [x](int arg) { return arg * x; };
}

auto generator_factory(int seed)
{
    return [seed]() mutable { return ++seed; };
}

int main()
{
    function<string (int)> l1 = [](int value)->string {
                if (value % 2 == 0)
                    return "Even";
                else
                    return string("Odd");
               };

    cout << "result = " << l1(7) << endl;

    vector<int> numbers = { 1, 2, 3, 4, 5, 6, 7 };
    vector<string> evens;

    transform(numbers.begin(), numbers.end(), back_inserter(evens), l1);

    for(size_t i = 0; i < numbers.size(); ++i)
    {
        cout << numbers[i] << " - " << evens[i] << endl;
    }

    int threshold = 5;

    auto gt_5 = [threshold](int x) { return x > threshold; };

    threshold = 6;

    auto it = find_if(numbers.begin(), numbers.end(), gt_5);

    if (it != numbers.end())
        cout << ">5 = " << *it << endl;

    int sum = 0;

    for_each(numbers.begin(), numbers.end(), [&sum](int x) { sum += x;});

    cout << "sum: " << sum << endl;

    auto multiplier_by_5 = function_functory(5);

    cout << "5 * 2 = " << multiplier_by_5(2) << endl;

    function<int()> gen1 = generator_factory(100);

    cout << "gen1: ";
    for(int i = 0; i < 15; ++i)
        cout << gen1() << " ";
    cout << endl;

    auto comp_by_value = [](shared_ptr<int> a, shared_ptr<int> b) { return *a < *b; };
    set<shared_ptr<int>, decltype(comp_by_value)> dict(comp_by_value);
    dict.insert({ make_shared<int>(6), make_shared<int>(5), make_shared<int>(2), make_shared<int>(10) });

    cout << "dict: ";
    for(const auto& ptr : dict)
        cout << *ptr << " ";
    cout << endl;

    unique_ptr<Aggregator> agg = make_unique<Aggregator>();

    {
        vector<int> nbs { 1, 2, 3 };

        //auto super_agg = [agg = move(agg)](int x) { agg->cumulate(x); };

        for_each(nbs.begin(), nbs.end(),
                 //[agg = move(agg)](int x) { agg->cumulate(x); });

                 //bind([](const unique_ptr<Aggregator>& agg, int value) { agg->cumulate(value);},
                 //     move(agg), placeholders::_1));

                 bind(&Aggregator::cumulate, move(agg), placeholders::_1));


        // C++11 version
        //auto lmv = bind([](const unique_ptr<Aggregator>& ptr){ ptr->foo(); }, move(ld));
    }

    cout << "after scope" << endl;
}
