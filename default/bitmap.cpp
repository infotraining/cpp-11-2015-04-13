#include "bitmap.hpp"

#include <vector>


struct Bitmap::Impl
{
    std::vector<uint8_t> pixels_;
};

Bitmap::Bitmap() : impl_{new Impl()}
{
}

Bitmap::~Bitmap() = default;
