#include <iostream>
#include <map>
#include <set>
#include "bitmap.hpp"

using namespace std;

void integer_only(int value)
{
    cout << "integer_only(" << value << ")" << endl;
}

template <typename T>
void integer_only(T) = delete;


class EvilCtors
{
    int x, y;
public:
    EvilCtors(int x)
    {
        cout << "EvilCtors(" << x << ")" << endl;
    }

    EvilCtors(int x, int y) : EvilCtors{x}
    {
        throw std::runtime_error("Error");
        cout << "ExliCtor(" << x << ", " << y << ")" << endl;
    }

    ~EvilCtors()
    {
        cout << "~EvilCtors()" << endl;
    }
};

class StackOnly
{
    static int seed_;
    int id_ = gen_id();
    string name_ {"Unkown"};
public:
    StackOnly() = default;

    StackOnly(const string& name) : StackOnly{gen_id(10), name}
    {}

    StackOnly(int id, const string& name) : id_(id), name_(name)
    {}

    void* operator new(size_t) = delete;
    void operator delete(void*) = delete;

    static int gen_id(int offset = 1)
    {
        seed_ += offset;
        return seed_;
    }

    void print() const
    {
        cout << "SO(" << id_ << ", " << name_ << ")" << endl;
    }
};

struct Color
{
    int r, g, b;

    Color(int r, int g, int b) : r(r), g(g), b(b)
    {}
};

struct Point
{
    int x = 10;
    int y;
    Color color;

    Point() = delete;
};



//Point pt = { 1, 0, {255, 255, 0} };

int StackOnly::seed_ = 0;

class StackOnlyDerived : public StackOnly
{

};

class indexed_set : public set<string>
{
public:
    using set<string>::set;

    const string& operator[](size_t index) const
    {
        auto it = begin();
        advance(it, index);

        return *it;
    }
};


int main() try
{
    indexed_set words = { "one", "two", "three" };

    cout << words[1] << endl;

    EvilCtors ec(1, 2);

    map<int, string> dict = { {1, "one"}, {2, "two"} };

    StackOnly so;
    so.print();

    StackOnly so2{"mp3 player"};
    so2.print();

    StackOnly so3{"gadget"};
    so3.print();

    //auto ptr = new StackOnlyDerived();

    integer_only(7);

    long lx = 88L;

//    integer_only(lx);

//    integer_only(9.88);

//    integer_only(77.6F);

//    integer_only("fsdfs");


    Bitmap bmp;

    //Bitmap copy_bmp = std::move(bmp);
}
catch(const exception& e)
{
    cout << e.what() << endl;
}
