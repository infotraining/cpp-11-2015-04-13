#ifndef BITMAP_HPP
#define BITMAP_HPP

#include <memory>

class Bitmap
{
    class Impl;

    std::unique_ptr<Impl> impl_;
public:
    Bitmap();

    Bitmap(const Bitmap&) = delete;
    Bitmap& operator=(const Bitmap&) = delete;

    ~Bitmap();
};


#endif // BITMAP_HPP
