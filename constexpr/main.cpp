#include <iostream>
#include <array>

using namespace std;

int x1 = 8;
constexpr int x2 = 7;

//constexpr int x3 = x1;

constexpr int factorial(int n)
{
    return (n == 0) ? 1 : factorial(n-1)*n;
}

constexpr int fibonacci(int n)
{
    return (n <= 2) ? 1 : fibonacci(n-1) + fibonacci(n-2);
}

int main()
{
    static_assert(fibonacci(9) == 34, "ERROR");

    for(int i = 1; i < 10; ++i)
        cout << "fibonacci(" << i << ") = " << fibonacci(i) << endl;

    array<int, factorial(8)> tab;

    cout << tab.size() << endl;

    cout << factorial(8) << endl;

    int x = 8;

    cout << factorial(x) << endl;
}
