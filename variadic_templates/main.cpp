#include <iostream>
#include <type_traits>
#include <vector>
#include <numeric>
#include <array>

using namespace std;

void print()
{
    cout << endl;
}

template <typename T, typename... Types>
void print(const T& arg1, Types... args)
{
    static_assert(is_arithmetic<T>::value, "T must be arithmetic");
    cout << arg1 << " ";
    print(args...);
}

template <typename... Types>
struct Count;

template <typename Head, typename... Tail>
struct Count<Head, Tail...>
{
    constexpr static int value = 1 + Count<Tail...>::value;
};

template <>
struct Count<>
{
    constexpr static int value = 0;
};

template <typename... Types>
struct VerifyCount
{
    static_assert(Count<Types...>::value == sizeof...(Types),
                  "Error in count");
};

template <typename... Base>
struct Derived : public Base...
{

};

class Car {};
class Boat {};

using Amph = Derived<Car, Boat>;

template<typename... Args1>
struct zip
{
    template<typename... Args2>
    struct with
    {
        using type = tuple<pair<Args1, Args2>...>;
    };
};

struct Details
{
    template <typename T>
    constexpr static T sum(T&& arg)
    {
        return arg;
    }

    template <typename T, typename... Args>
    constexpr static auto sum(T&& arg , Args&&... args) -> decltype(forward<T>(arg) + sum(forward<Args>(args)...))
    {
        return  forward<T>(arg) + sum(forward<Args>(args)...);
    }
};


template <typename... Args>
constexpr auto avg(Args&&... args) -> decltype(Details::sum(args...))
{
    return Details::sum(forward<Args>(args)...) / sizeof...(args);
}

template <typename T, typename... Tails>
T sum_alt(const T& first, Tails... args)
{
    array<T, sizeof...(Tails)+1> vec {args...};

    return accumulate(vec.begin(), vec.end(), first);
}

int main()
{
    print(1, 2, 4, 44.5, 55.44F);

    VerifyCount<int, double, char> {};


    auto avg1 = avg(1.0, 3.0, 3);

    cout << "avg1:" << " = " << avg1 << endl;

    cout << "sum = " << sum_alt(1.5, 3.6, 3) << endl;
}
