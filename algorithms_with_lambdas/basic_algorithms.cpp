#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>
#include <random>

using namespace std;

int main()
{
    vector<int> vec(25);

    //std::random_device rd;
    std::mt19937 mt {0};
    std::uniform_int_distribution<int> uniform_dist {1, 30};

    generate(vec.begin(), vec.end(), [&] { return uniform_dist(mt); } );

    cout << "vec: ";
    for(const auto& item : vec)
        cout << item <<" ";
    cout << endl;

    auto is_even = [](int x) {  return x % 2 == 0; };

    // 1a - wyświetl parzyste
    copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "),
            is_even);
    cout << endl;

    // 1b - wyswietl ile jest parzystych
    cout << "evens: " << count_if(vec.begin(), vec.end(), is_even) << endl;

    int eliminators[] = { 3, 5, 7 };
    // 2 - usuń liczby podzielne przez dowolną liczbę z tablicy eliminators
    vec.erase(remove_if(vec.begin(), vec.end(),
                        [&eliminators](int x) {
                            return any_of(begin(eliminators), end(eliminators),
                                          [x](int div) { return x % div == 0;});
                        }), vec.end());

    cout << "po usunieciu podzielnych przez eliminators: ";
    for(const auto& item : vec)
        cout << item << " ";
    cout << endl;

    // 3 - tranformacja: podnieś liczby do kwadratu
    transform(vec.begin(), vec.end(), vec.begin(), [](int x) { return x * x;});

    cout << "po transformacji do kwadratu: ";
    for(const auto& item : vec)
        cout << item << " ";
    cout << endl;


    // 4 - wypisz 5 najwiekszych liczb
    cout << "5 najwiekszych: ";

    nth_element(vec.begin(), vec.begin() + 5, vec.end(), greater<int>());
    for(auto it = vec.begin(); it != vec.begin() + 5; ++it)
        cout << *it << " ";
    cout << endl;

    // 5 - policz wartosc srednia
    int sum = 0;

    for_each(vec.begin(), vec.end(), [&sum](int x) { sum += x; });
    double avg = sum / static_cast<double>(vec.size());

    cout << "avg = " << avg << endl;

    // 6 - utwórz dwa kontenery - 1. z liczbami mniejszymi lub równymi średniej, 2. z liczbami większymi od średniej
    // TODO
    vector<int> less_eq_avg;
    vector<int> gt_avg;

    partition_copy(vec.begin(), vec.end(), back_inserter(gt_avg), back_inserter(less_eq_avg),
                   [avg](int x) { return x > avg; });

//                   bind(greater<int>(),
//                            placeholders::_1, avg));


    cout << "less_eq_avg: ";
    for(const auto& item : less_eq_avg)
        cout << item << " ";
    cout << endl;

    cout << "gt_avg: ";
    for(const auto& item : gt_avg)
        cout << item << " ";
    cout << endl;

}
