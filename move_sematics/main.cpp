#include <iostream>
#include <memory>
#include <vector>

using namespace std;

void may_throw()
{
    throw std::runtime_error("Error");
}

class Moveable
{
    int* data_;
public:
    Moveable(int data) : data_{new int{data}}
    {
        cout << "Moveable(" << *data_ << ")" << endl;

    }

    ~Moveable()
    {
        if (data_)
            cout << "~Moveable(" << *data_ << ")" << endl;
        else
            cout << "~Moveable(nullptr)" << endl;
        delete data_;
    }

    Moveable(const Moveable& source) : data_{new int{*source.data_}}
    {
        cout << "Moveable(const Moveable& " << *data_ << ")" << endl;
    }

    Moveable& operator=(const Moveable& source)
    {
        if (this != &source)
        {
            cout << "op=(const Moveable& " << *source.data_ << ")" << endl;
            int* temp = new int{*source.data_};
            delete data_;

            data_ = temp;
        }

        return *this;
    }

    Moveable(Moveable&& rsource) noexcept : data_ {rsource.data_}
    {
        //cout << "Moveable(Moveable&& " << *data_ << ")" << endl;

        rsource.data_ = nullptr;
    }

    Moveable& operator=(Moveable&& rsource) noexcept
    {
        if (this != &rsource)
        {
            delete data_;
            data_ = rsource.data_;
            rsource.data_ = nullptr;

            //cout << "op=(Moveable&& " << *data_ << ")" << endl;
        }

        return *this;
    }

    int data() const
    {
        return *data_;
    }
};

class Broker
{
    Moveable mv_;
public:
    Broker(int data) : mv_{data}
    {}

    virtual ~Broker()
    {
        cout << "~Broker()" << endl;
    }

    Broker(Broker&&) = default;
    Broker& operator=(Broker&&) = default;
};

class UberBroker : public Broker
{
    Moveable mv_;
public:
    UberBroker(int data1, int data2) : Broker{data1}, mv_{data2}
    {}

    UberBroker(Moveable&& mv) : Broker{mv.data()}, mv_{move(mv)}
    {}

    UberBroker(const UberBroker&) = delete;
    UberBroker& operator=(const UberBroker&) = delete;

    UberBroker(UberBroker&&) = default;
    UberBroker& operator=(UberBroker&&) = default;
};

UberBroker factory(int a, int b)
{
    UberBroker ub{a, b};

    // ...

    return ub;
}

void have_fun(Moveable& m)
{
    cout << "have_fun(Moveable&)" << endl;
}

void have_fun(const Moveable& m)
{
    cout << "have_fun(const Moveable&)" << endl;
}

void have_fun(Moveable&& m)
{
    cout << "have_fun(Moveable&&)" << endl;
}

template <typename T>
void use(T&& arg)
{
    have_fun(forward<T>(arg));
}

int main()
{
//    {
//        const Moveable mv1{10};
//        auto mv2 = Moveable{move(mv1)};
//    }

//    cout << "\n\n";

//    {
//        UberBroker ub40{1, 2};

//        UberBroker ub42 = factory(5, 6);
//    }

//    const Moveable cmv{1};
//    Moveable mv{2};

//    //use(cmv);
//    //use(mv);
//    use(Moveable{3});

    vector<Moveable> vec;
    //vec.reserve(10);

    vec.push_back(Moveable{1});

    Moveable mv{2};
    vec.push_back(mv);

    vec.emplace_back(3);

    cout << "\n\n";

    pair<int, int> p;
}
