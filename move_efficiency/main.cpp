#include <iostream>
#include <vector>
#include <list>
#include <deque>
#include <chrono>
#include <cstring>
#include <algorithm>
#include <memory>

using namespace std;

namespace MoveSemantics
{
    namespace OnlyCopyable
	{
		class ExpensiveObject
		{
		private:
		    char* data_;
		    size_t size_;
		public:
		    ExpensiveObject(const char* data) : data_(nullptr), size_(std::strlen(data))
		    {
		        data_ = new char[size_ + 1];
		        std::strcpy(data_, data);
		    }

		    ExpensiveObject(const ExpensiveObject& source) : data_(nullptr), size_(source.size_)
		    {
		        data_ = new char[size_+1];
		        std::strcpy(data_, source.data_);
		    }

		    ExpensiveObject& operator=(const ExpensiveObject& source) 
		    {
		        ExpensiveObject temp(source);
		        swap(temp);
		        return *this;
		    }

		    ~ExpensiveObject()
		    {
		        delete [] data_;
		    }

		    void swap(ExpensiveObject& other)
		    {
		        std::swap(data_, other.data_);
		        std::swap(size_, other.size_);
		    }

		    const char* data() const
		    {
		        return data_;
		    }

		    size_t size() const
		    {
		        return size_;
		    }

		    bool operator<(const ExpensiveObject& other) const
		    {
		        if (std::strcmp(data_, other.data_) < 0)
		            return true;
		        return false;
		    }
		};
	}

    inline namespace OnlyMoveable
	{
		class ExpensiveObject
		{
		private:
		    char* data_;
		    size_t size_;
		public:
		    ExpensiveObject(const char* data) : data_(nullptr), size_(std::strlen(data))
		    {
		        data_ = new char[size_ + 1];
		        std::strcpy(data_, data);
		    }

		    ExpensiveObject(ExpensiveObject&& rsource) noexcept :
		        data_(rsource.data_), size_(rsource.size_)
		    {
		        rsource.data_ = nullptr;
		        rsource.size_ = 0;
		    }

		    ExpensiveObject& operator=(ExpensiveObject&& rsource) noexcept
		    {
		    	if (this != &rsource)
		    	{
			    	delete [] data_;
			    	data_ = rsource.data_;
			    	size_ = rsource.size_;

			    	rsource.data_ = nullptr;
			    	rsource.size_ = 0;
		    	}
		    	return *this;
		    }

		    ~ExpensiveObject()
		    {
		        delete [] data_;
		    }

		    void swap(ExpensiveObject& other)
		    {
		        std::swap(data_, other.data_);
		        std::swap(size_, other.size_);
		    }

		    const char* data() const
		    {
		        return data_;
		    }

		    size_t size() const
		    {
		        return size_;
		    }

		    bool operator<(const ExpensiveObject& other) const
		    {
		        if (std::strcmp(data_, other.data_) < 0)
		            return true;
		        return false;
		    }
		};
	}
}

using namespace MoveSemantics;

template<typename Cont>
void sort(Cont& c)
{
    sort(begin(c), end(c));
}	

template<typename T>
void sort(list<T>& c)
{
    c.sort();
}

template <typename RandGen>
ExpensiveObject expensive_object_generator(RandGen&& rand_gen)
{
    static char txt[] = "abcdefghijklmn";
    static const size_t size = std::strlen(txt);

    std::shuffle(txt, txt + size, std::forward<RandGen>(rand_gen));

    return ExpensiveObject(txt);
}


template<typename Cont>
void test(Cont& c, size_t size)
{
    auto start = chrono::high_resolution_clock::now();

    random_device rd;
    mt19937 rand_gen{rd()};
    for (size_t i = 0 ; i < size ; ++i)
    {
        c.push_back(expensive_object_generator(rand_gen));
    }

    auto end1 = chrono::high_resolution_clock::now();
    cout << "Creating, size " << size << " = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end1-start).count();
    cout << " us" << endl;

    sort(c);

    auto end2 = chrono::high_resolution_clock::now();
    cout << "Sorting, size " << size << " = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end2-end1).count();
    cout << " us" << endl;
}



int main()
{
    // create vector, list, deque 10000 ;
    // and try to sort it
    // and time it

    const int N = 100000;
    auto test_range = { N, N*10, N*100 };

    cout << "Vector" << endl;
    vector<ExpensiveObject> vec;
    for (auto i : test_range)
    {
        test(vec, i);
        vec.clear();
        cout << "\n";
    }

    cout << "\n\n";

    cout << "List" << endl;
    list<ExpensiveObject> l;
    for (auto i : test_range)
    {
        test(l, i);
        l.clear();
        cout << "\n";
    }

    cout << "\n\n";

    cout << "Deque" << endl;
    deque<ExpensiveObject> d;
    for (auto i : test_range)
    {
        test(d, i);
        d.clear();
        cout << "\n";
    }

    return 0;
}

