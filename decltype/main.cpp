#include <iostream>
#include <map>
#include <typeinfo>
#include <memory>

using namespace std;

template <typename Map>
void print_map(const Map& m)
{
    cout << "[ ";
    for(const auto& kv : m)
    {
       cout << " { " << kv.first << " - " << kv.second << "} ";
    }
    cout << "]\n";
}

class Base
{
public:
    void do_sth()
    {}
};

class Derived : public Base
{

};

void foo(Base* ptr)
{
    using Type = decltype(*ptr);

    cout << typeid(Type).name() << endl;
}

shared_ptr<Base> global_ptr{ new Derived() };

void side_effect()
{
    global_ptr.reset();
}

void use(Base& ptr)
{
    side_effect();
    ptr.do_sth();
}

int main()
{
    map<int, string> m1 = { {1, "one"}, {2, "two"} };
    print_map(m1);

    decltype(m1) m2;
    print_map(m2);

    use(*pin);
    auto m3 = m1;
    print_map(m3);

    using ValueType = decltype(m3[1]);

    ValueType v = m3[1];

    cout << v << endl;

    auto ptr = shared_ptr<Base>{new Derived()};
    decltype(ptr) ptr2;

    foo(ptr.get());

    auto pin = global_ptr;
    use(*pin);
}
