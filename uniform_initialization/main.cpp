#include <iostream>
#include <vector>
#include <array>

using namespace std;

int get_value()
{
    return 0;
}

struct Evil
{
    int x, y;

    Evil() = delete;
};


struct Point
{
    int x, y;

    Point() : x{get_value()}, y{get_value()}
    {
        cout << "Point(dflt ctor)" << endl;
    }

    Point(int x, int y) : x(x), y(y)
    {
        cout << "Point(ctor)" << endl;
    }

    Point(std::initializer_list<int> lst)
    {
        cout << "Point(initializer_list)" << endl;
        x = *lst.begin();
        y = *(lst.begin() + lst.size() - 1);
    }
};

template <typename Container>
void print(const Container& cont, const string& name)
{
    cout << name << ": [ ";
    for(typename Container::const_iterator it = cont.begin(); it != cont.end(); ++it)
    {
        cout << *it << " ";
    }
    cout << "]\n";
}

struct Aggregate
{
    int tab[2];
    int x, y;
};

class Value
{
    static int value_generator;
    int value_;
public:
    Value() : value_(value_generator++)
    {}

    int value() const
    {
        return value_;
    }
};

ostream& operator<<(ostream& out, const Value& v)
{
    out << v.value();

    return out;
}

int Value::value_generator{};

Aggregate agg1 { {1, 2}, 3, 4 };

class WithMember
{
    array<int, 10> member = { { 1, 2, 3, 4, 5 } };
    int x {10};
    vector<int> vec = {1, 2, 3};

    WithMember() = default;
};

int main()
{
    Evil ev{1, 2};

    Point pt1 {1, static_cast<int>(2.5)};

    cout << "pt1 = (" << pt1.x << ", " << pt1.y << ")" << endl;

    Point pt2{};

    cout << "pt1 = (" << pt2.x << ", " << pt2.y << ")" << endl;

    vector<int> vec1 = { 1, 2, 3, 4 };

    vec1.insert(vec1.begin(), {5, 6, 7, 8});

    print(vec1, "vec1");

    vector<int> vec2{1, 2, 3, 4};

    vector<int> vec3(10, 1);
    print(vec3, "vec3");

    vector<int> vec4{10, 1};
    print(vec4, "vec4");

    vector<Value> vec5 {10};
    print(vec5, "vec5");
}
